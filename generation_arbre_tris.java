//auteur LMG
//license GPL V3 : https://www.gaiac.eu/gpl.txt
//version: 0.1
import java.io.File;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

public class generation_arbre_tris {

   public static void main(String[] args) {
	// algorithme generant un arbre sous forme de fichier XML, associant l'ensemble des noms d'un fichiers, avec les sous repertoires qui l'heberge
	// note de version: 0.1 pour le moment seul les structures à 1 niveau sont prise en compte
      try {
         File inputFile = new File("base.xml");
         DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
         DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
         Document doc = dBuilder.parse(inputFile);
         doc.getDocumentElement().normalize();
         System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
         NodeList nList = doc.getElementsByTagName("theme");
         System.out.println("----------------------------");
         
         for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);
            System.out.println("\nNiveau :" + nNode.getNodeName());
            
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
               Element eElement = (Element) nNode;
               System.out.println("Nom de la classe : " 
                  + eElement.getAttribute("type"));
				int max = eElement.getElementsByTagName("nom").getLength();
				for (int temp2 = 0; temp2 < max; temp2++) {
               		System.out.println(" Element : " + eElement.getElementsByTagName("nom").item(temp2).getTextContent());
					}
            }
         }
      } catch (Exception e) {
         e.printStackTrace();
      }
   }
}
