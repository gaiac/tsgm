//auteur LMG
//license GPL V3 : https://www.gaiac.eu/gpl.txt
//version: 0.2
import java.util.ArrayList;

import java.io.File;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import org.xml.sax.SAXException;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

//import OutilsTris.CreerBalise;



class Generer {
	//modifier un fichier xml, stockant la sémantique du tris d'un ensemble de repertoire existant
	// note de version: 0.1 pour le moment seul les structures de 1 niveau sont prise en compte
	//commande bash: $/repertoire parents/javac Commande [nom des fichiers]
	public static void main(String[] args)
	{
	String[] repertoires=args;
	int s=0;
    final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

    try
	{
        final DocumentBuilder builder = factory.newDocumentBuilder();
		//charge le document XML existant
	    final Document document= builder.parse(new File("repertoire.xml"));
        final NodeList node_classement = document.getElementsByTagName("classement");
		//prend l item 0, pour la racine, car la structure de donnee standard prévoir qu il n y a que un element racine
		final Element racine = (Element)node_classement.item(0);
		Element encours_balise=null;
		Element encours_nom=null;
		boolean ecrire=false;
		//exemple #3
		encours_nom=OutilsTris.ChercherTag(document, racine, "theme","type","bonneappetit/");
		if (! (encours_nom==null))
		{
			OutilsTris.CreerBalise(document,encours_nom,"nom","bye");
			ecrire= true;
		}

		//exemple #1 incrementation recursive (genere repertoire-0.2xml)
		//encours_nom=OutilsTris.CreerTag(document, racine, "theme", "type", "bonjour");
		//encours_nom=OutilsTris.CreerTag(document, encours_nom, "theme", "type", "bonnejournee");
		//encours_nom=OutilsTris.CreerTag(document, encours_nom, "theme", "type", "bonneapetit");
		//encours_nom=OutilsTris.CreerTag(document, encours_nom, "theme", "type", "bonneapresmidi");
		//encours_nom=OutilsTris.CreerTag(document, encours_nom, "theme", "type", "joyeuxgouter");
		//if (! (encours_nom==null))
		//{
		//	OutilsTris.CreerBalise(document,encours_nom,"nom","ligne");
		//	ecrire= true;
		//}

		//exemple #2 d'utilisation avec cherchertag
		//OutilsTris.CreerTag(document, racine, "theme", "type", "bonjour");
		//ecrire=(! (encours_balise==null)|| ecrire);	//demander la mise à jour
		//encours_nom=OutilsTris.ChercherTag(document, racine, "theme","type","bonjour");
		//if (! (encours_nom==null))
		//{
		//	OutilsTris.CreerBalise(document,encours_nom,"nom","ligne");
		//	ecrire= true;
		//}
		//encours_nom=OutilsTris.ChercherTag(document, racine, "theme","type","sport");
		//if (! (encours_nom==null))
		//{
		//	OutilsTris.CreerBalise(document,encours_nom,"nom","e");
		//	ecrire= true;			
		//}

		if(ecrire)
		{
			OutilsTris.maj_fichier_xml("repertoires.xml",document);
		}//ecrire
	}//try

	catch (final ParserConfigurationException e)
	{
		e.printStackTrace();
	}
	catch (final SAXException e)
	{
		e.printStackTrace();
	}
	catch (final IOException e)
	{
		e.printStackTrace();
	}

	}
}
