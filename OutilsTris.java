//auteur LMG
//license GPL V3 : https://www.gaiac.eu/gpl.txt
//version: 0.2
import java.util.ArrayList;
import java.util.StringTokenizer;

//import java.io.File;
import java.io.*;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
// V0.
//	<racine>
//		<nom_tag nom_type=value_type>
//			<nom_balise>value_balise</nom_balise>
//		</nom_tag>
//	</racine>
// V0.2
//	<racine>
//		<nom_tag nom_type=value_type>
//			<nom_tag2 nom_type2=value_type2>
//				<nom_balise>value_balise</nom_balise>
//				<nom_balise2>value_balise2</nom_balise2>
//			</nom_tag2>
//		</nom_tag>
//		<nom_tag1 nom_type1=value_type1>
//			<nom_balise1>value_balise1</nom_balise1>
//		</nom_tag1>
//	</racine>


class OutilsTris
	{
	public OutilsTris()
	{
		
	}
	public static ArrayList<String> ajout(String str)
	{
		//allocation de mémoire pour un tableau
		ArrayList<String> StrZ = new ArrayList<String>(){{ add(str);}};
		
		return StrZ;
	}

	public static ArrayList<ArrayList <String>> RecupXml(String fichiers)
	{
		//allocation de mémoire pour un tableau de tableau
		ArrayList<ArrayList <String>> StrA = new ArrayList<ArrayList <String>>();

		try
		{
			//Declaration tableau pour manipulation
			ArrayList<String> StrB; 

			//ouverture base.xml par defaut
			File inputFile = new File(fichiers);

			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();

			//cherche la racine du squelette XML "theme"
			NodeList nList = doc.getElementsByTagName("theme");

			
			for (int temp = 0; temp < nList.getLength(); temp++)
			{
				//declaration d'un noeud xml (temp est la variable d'index)
				Node nNode = nList.item(temp);
				
				if (nNode.getNodeType() == Node.ELEMENT_NODE)
				{
					//Declaration du noeud xml
					Element eElement = (Element) nNode;
					//Declaration du nombre maximum d'élement
					int max = eElement.getElementsByTagName("nom").getLength();
	
					//créer un nouvelle element dans le tableau(allocation mémoire), avec pour premiere cellule le nom du "theme"
					StrA.add(ajout(eElement.getAttribute("type")));
					//une fois l'allocation de mémoire du tableau effectué, récuperation de la cellule pour exploitation
					StrB=StrA.get(StrA.size()-1);


					//(temp2 est la variable d'index du "tableau" eElement)
					for (int temp2 = 0; temp2 < max; temp2++)
					{
						//implementé l'element de toute les tag implementé dans le XML
						StrB.add(eElement.getElementsByTagName("nom").item(temp2).getTextContent());
					}
				}
			}
			//retourner le tableau final
			return StrA;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return StrA;
		}
	}

	public static ArrayList<ArrayList <String>> RecupXml()
	{
		return RecupXml("base.xml");
	}

	public static Element CreerTag(Document document, Element racine, String nom_balise, String nom_tag, String value_tag)
	{
	    final Element theme = document.createElement(nom_balise);

		theme.setAttribute(nom_tag, value_tag);
	    racine.appendChild(theme);
		return theme;
	}
	//
	public static void CreerBalise(Document document, Element encours, String nom_balise, String value_balise)
	{
		//	System.out.println(encours_nom.getAttribute("type")); //bavard niv 1
		    Element nom = document.createElement(nom_balise);
			nom.appendChild(document.createTextNode(value_balise));
			encours.appendChild(nom);
//			racine.appendChild(encours);

//			return document;
	}

	//retourne Element nom_balise comprenant nom_tag et value_tag ou null si n existe pas
	public static Element ChercherTag(Document document, Element racine, String nom_balise, String nom_tag,String value_tag)
	{//1
		//Déclarations
		final NodeList node_classement = document.getElementsByTagName(nom_balise);
		Element Element_encours;
		Element Trouve=null;
		String balise_type;
		boolean ok;
		ok=false;
		int max= node_classement.getLength();
		String value_tag_encours;

		if (node_classement.getLength() > 0)
		{//2
			for (int temp = 0; temp < max; temp++)
			{//3
				Element_encours = (Element)node_classement.item(temp);
				balise_type=Element_encours.getAttribute("type");
				if ( ( value_tag.indexOf("/") > 1) )
				{ //4
					StringTokenizer index_value_tag=new StringTokenizer(value_tag,"/");
					if (index_value_tag.hasMoreTokens() )
					{//5
						value_tag_encours=index_value_tag.nextToken();
						System.out.print(">" + value_tag_encours);
						if (value_tag_encours.equals(balise_type))
						{//6
							ok=true;
							//Trouve=Element_encours; //ver 0.1
							System.out.print("#");
							//affiche la valeur du tag de l'element en cours
							System.out.print(Trouve.getAttribute(nom_tag) + "@1@"); //#bavard niv1
							System.out.print(":"); //#bavard niv 1
							//affiche toute les balises comprise dans l'elements
							System.out.print(Trouve.getTextContent()); //#bavard niv 1
							System.out.println(":"); //#bavard niv 1
							Trouve=Element_encours;
						}//6
						else
						{//7
							Trouve=ChercherTag(document, racine, nom_balise, nom_tag, index_value_tag.toString()) ;
						}//7

					}//5
				}//4
			}//3
		}//2
	return Trouve;
	}//1

	public static boolean maj_fichier_xml(String fichier, Document document)
	{
		try
		{
			final TransformerFactory transformerFactory = TransformerFactory.newInstance();
			final Transformer transformer = transformerFactory.newTransformer();
			final DOMSource source = new DOMSource(document);
			final StreamResult sortie = new StreamResult(new File("repertoires.xml"));

			//prologue
			transformer.setOutputProperty(OutputKeys.VERSION, "1.0");
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");

			//formatage
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

			//sortie
			transformer.transform(source, sortie);
		}
		catch (TransformerConfigurationException e)
		{
		    e.printStackTrace();
		}
		catch (TransformerException e)
		{
		    e.printStackTrace();
		}
		return true;
	}

}
