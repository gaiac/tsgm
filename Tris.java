//auteur LMG
//license GPL V3 : https://www.gaiac.eu/gpl.txt
//version: 0.3
import java.util.ArrayList;
import appli.OutilsTris;


class Tris {
	//lie un fichier xml, stockant la sémantique du tris d'un ensemble de repertoire existant, et deplace les fichiers du repertoire en cours dans les repertoires reperés dans le fichier xml
	// note de version: 0.1 pour le moment seul les structures de 1 niveau sont prise en compte
	//commande bash: $/repertoire parents/javac Tris [nom des fichiers]
	public static void main(String[] args)
	{
		OutilsTris blabla= new OutilsTris();
		// pour chaque elements passés en paramètre (si *, tous les fichiers du repertoire)
		for( String x: args)
		{
			// on verifie pour chaque themes
			for ( ArrayList<String> y:blabla.RecupXml() )
			{
				// à chaque atome
				for ( String z:y )
				{
					// si l'element contient un des atomes de la base xml
					if (x.toLowerCase().contains(z))
					{
						ProcessBuilder builder = new ProcessBuilder();
						//action à chacun des atomes trouvé
						System.out.println("mv '"+x+"' '/run/media/gaiac/66872158-5776-4d61-8187-83df2ab62620/"+y.get(0)+"/"+x+"'");
						//Runtime.getRuntime().exec(String.format("mv '"+x+"' '/run/media/gaiac/2d64daf8-32d8-45b2-8e12-e5548867f85b/plusplus/1/"+y.get(0)+"'"));
						//builder.command("mv '"+x+"' '/run/media/gaiac/66872158-5776-4d61-8187-83df2ab62620/"+y.get(0)+"/"+x+"'");
						break;
					}
					else
					{
						//action à chacun atomes non trouvé
						//System.out.println("mv "+x+" /run/media/gaiac/2d64daf8-32d8-45b2-8e12-e5548867f85b/plusplus/1/atrier");
					}
			 	}
			}
		}
	}
}
